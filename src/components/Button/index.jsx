import "./index.scss"
import { CircularProgress } from "@material-ui/core"

export default function Button({
  className,
  style,
  children,
  icon: Icon,
  color = "primary",
  loading = false,
  shape = "filled",
  borderWidth = 2,
  position = "left",
  size = "medium",
  borderType = "rounded",
  disabled = false,
  ...rest
}) {
  if (shape === "filled") {
    return (
      <button
        disabled={disabled || loading}
        type="button"
        style={style}
        className={`
            button
            ${className}
            focus:outline-none
            transition
            ${children ? "" : "w-9 h-9"}
            focus:ring focus:border-blue-300
            focus-within:z-40
            ${
              color === "gray"
                ? "bg-gray-600 iconColor-filled hover:opacity-90 border-2 border-gray-600"
                : "bg-primary-600 iconColor-filled hover:opacity-90 border-2 border-primary-600"
            }
            ${
              size === "small"
                ? "h-6 px-2 py-0 text-sm rounded"
                : size === "medium"
                ? "h-8 px-3 py-1 min:w-7 text-sm rounded-md"
                : "px-4 py-2 rounded-md"
            }
            ${borderType === "rectangle" && "rounded-none"}
            ${disabled || loading ? "disabled" : ""}
            text-white
          `}
        {...rest}
      >
        <div
          className={`flex items-center justify-center ${
            children ? "space-x-2" : ""
          } font-medium`}
        >
          {loading && <CircularProgress size={16} color="#fff" />}
          {Icon && position === "left" && <Icon style={{ fontSize: "18px" }} />}
          <div className="text-sm">{children}</div>
          {Icon && position === "right" && (
            <Icon style={{ fontSize: "18px" }} />
          )}
        </div>
      </button>
    )
  }

  if (shape === "outlined") {
    return (
      <button
        disabled={disabled || loading}
        type="button"
        style={style}
        className={`
            button
            ${className}
            focus:outline-none
            transition
            ${children ? "" : "w-9 h-9"}
            focus:ring focus:border-blue-300
            focus-within:z-40
            ${
              color === "gray"
                ? "bg-transparent text-gray-600 border-2 border-gey-600 hover:bg-background_2"
                : "bg-transparent text-primary-600 border-2 border-primary-600 hover:bg-background_2"
            }
            ${
              size === "small"
                ? "h-6 px-2 py-0 text-sm rounded"
                : size === "medium"
                ? "h-8 px-3 py-1 min:w-7 text-sm rounded-md"
                : "px-4 py-2 rounded-md"
            }
            ${borderType === "rectangle" && "rounded-none"}
            ${disabled || loading ? "disabled" : ""}
            text-white
          `}
        {...rest}
      >
        <div
          className={`flex items-center justify-center ${
            children ? "space-x-2" : ""
          } font-medium`}
        >
          {loading && <CircularProgress size={16} color="#fff" />}
          {Icon && position === "left" && <Icon style={{ fontSize: "18px" }} />}
          <div className="text-sm">{children}</div>
          {Icon && position === "right" && (
            <Icon style={{ fontSize: "18px" }} />
          )}
        </div>
      </button>
    )
  }
  if (shape === "text") {
    return (
      <button
        disabled={disabled || loading}
        type="button"
        style={style}
        className={`
            button
            ${className}
            focus:outline-none
            transition
            ${children ? "" : "w-9 h-9"}
            focus:ring focus:border-blue-300
            focus-within:z-40
            ${
              color === "gray"
                ? "bg-transparent text-gray-600 hover:opacity-90 border-2 border-white"
                : "bg-transparent text-primary-600 hover:opacity-90 border-2 border-white"
            }
            ${
              size === "small"
                ? "h-6 px-2 py-0 text-sm rounded"
                : size === "medium"
                ? "h-8 px-3 py-1 min:w-7 text-sm rounded-md"
                : "px-4 py-2 rounded-md"
            }
            ${borderType === "rectangle" && "rounded-none"}
            ${disabled || loading ? "disabled" : ""}
            text-white
          `}
        {...rest}
      >
        <div
          className={`flex items-center justify-center ${
            children ? "space-x-2" : ""
          } font-medium`}
        >
          {loading && <CircularProgress size={16} color="#fff" />}
          {Icon && position === "left" && <Icon style={{ fontSize: "18px" }} />}
          <div className="text-sm">{children}</div>
          {Icon && position === "right" && (
            <Icon style={{ fontSize: "18px" }} />
          )}
        </div>
      </button>
    )
  }
}
