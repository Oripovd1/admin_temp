export default function Layout(props) {
  return (
    <div
      className="flex bg-background w-full"
      style={{ minHeight: "100vh", height: "inherit" }}
    >
      <div className="flex-none">{props.sidebar}</div>
      <div
        className="flex-grow"
        style={{
          height: "100vh",
          overflowY: "auto",
          overflowX: "hidden",
          position: "relative",
        }}
      >
        <div>{props.children}</div>
      </div>
    </div>
  )
}
