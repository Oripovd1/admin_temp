import { useState, useEffect } from "react"
import { useHistory, NavLink, Link } from "react-router-dom"
import config from "../../config/defaultSettings"
import Tooltip from "@material-ui/core/Tooltip"
import { makeStyles } from "@material-ui/core/styles"
import Avatar from "../Avatar/Index"
import "./index.scss"
import menu from "./menu"
import iconFinder from "../../constants/icons"
import { useTranslation } from "react-i18next"
import { brandLogo } from "../../assets/icons/icons"
import { useDispatch, useSelector } from "react-redux"
import { CLEAR_ON_SIGNOUT } from "../../redux/constants"
import LogoutModal from "./Modal"

const logoutIcon = (
  <svg
    width="20"
    height="18"
    viewBox="0 0 20 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M15 4L13.59 5.41L16.17 8H6V10H16.17L13.59 12.58L15 14L20 9L15 4ZM2 2H10V0H2C0.9 0 0 0.9 0 2V16C0 17.1 0.9 18 2 18H10V16H2V2Z"
      fill="#6E8BB7"
    />
  </svg>
)

const useStylesBootstrap = makeStyles((theme) => ({
  arrow: {
    color: theme.palette.common.black,
  },
  tooltip: {
    backgroundColor: theme.palette.common.black,
    fontSize: 12,
  },
}))

function BootstrapTooltip(props) {
  const classes = useStylesBootstrap()

  return <Tooltip placement="right" arrow classes={classes} {...props} />
}

export default function Sidebar() {
  // **** USE-HOOKS ****
  const history = useHistory()
  const { t } = useTranslation()
  const dispatch = useDispatch()

  const login = useSelector((state) => state.auth.login)

  const [anchorEl, setAnchorEl] = useState(false)
  const [visible, setVisible] = useState(false)
  const [selectedList, setSelectedList] = useState([])
  const [isModalOpen, setIsModalOpen] = useState(false)
  const toggleSidebar = () => setVisible((prev) => !prev)

  const logoutHandler = () => {
    setIsModalOpen(true)
  }

  useEffect(() => {
    if (menu.length) {
      menu.forEach((el) => {
        const fatherPathname = el.path.split("/")[2]
        if (history.location.pathname.includes(fatherPathname)) {
          if (el.children && el.children.length) {
            setSelectedList(el.children)
            setVisible((prev) => true)
          }
        }
      })
    }
  }, [history])

  // **** FUNCTOINS ****
  const linkTo = (item) => {
    if (item.isChild) {
      setVisible((prev) => true)
      history.replace(item.path)
      return
    }
    if (item.children && item.children.length) {
      setVisible((prev) => true)
      setSelectedList(item.children)
      return
    }
    setVisible((prev) => false)
    return item.path
  }

  // **** COMPONENTS ****
  const RenderSidebarItems = ({ items }) => {
    return (
      <ul className="space-y-2 text-sm mt-5 dashboard_list font-body">
        {items.map((el) => (
          <li key={el.id}>
            <NavLink
              activeClassName="is-active"
              onClick={() => linkTo(el)}
              to={el.path}
            >
              <span
                className={`spanITem flex items-center space-x-3 text-gray-700 p-3 hover:text-black rounded-md font-medium hover:bg-background_2 focus:shadow-outline`}
              >
                <span>{t(el.title)}</span>
              </span>
            </NavLink>
          </li>
        ))}
      </ul>
    )
  }

  return (
    <div className="flex h-screen h-full" style={{ height: "100%" }}>
      <LogoutModal
        isOpen={isModalOpen}
        close={() => setIsModalOpen(false)}
        logout={() => dispatch({ type: CLEAR_ON_SIGNOUT })}
      />
      <div
        className="font-body flex flex-col items-center fixed top-0 bottom-0 left-0 justify-between bg-white"
        style={{ borderRight: "1px solid rgba(229, 233, 235, 0.75)" }}
      >
        <div>
          <ul className="px-auto mt-2 pb-2 flex" onClick={toggleSidebar}>
            <Avatar size={40} color="transparent" className="mx-auto">
              {brandLogo}
            </Avatar>
          </ul>
          <div className="border-b"></div>
          <ul className="space-y-1 w-full text-sm items-center">
            {menu.map((el) => (
              <li key={el.id}>
                <NavLink
                  exact={false}
                  activeClassName="is-active-sidebar"
                  onClick={() => linkTo(el)}
                  to={el.path}
                >
                  <BootstrapTooltip title={t(el.title)}>
                    <span
                      className={`active-sidebar w-16 h-14 flex items-center justify-center space-x-2 text-gray-700 p-0 rounded-md`}
                    >
                      <span className="text-secondary flex items-center">
                        {iconFinder(el.icon)}
                      </span>
                    </span>
                  </BootstrapTooltip>
                </NavLink>
              </li>
            ))}
          </ul>
        </div>

        <div className="flex flex-col items-center space-y-5">
          <ul
            style={{ transition: "all 0.3s" }}
            className="space-y-2 items-end dashboard_list transition ease-in-out transform"
          >
            <hr className="py-1" />
            <div onClick={(e) => setAnchorEl((prev) => !prev)}>
              <Link to="/home/profile">
                <Avatar
                  aria-controls="simple-menu"
                  size={40}
                  alt=""
                  text={"A"}
                />
              </Link>
            </div>
            <div
              onClick={logoutHandler}
              className="flex justify-center cursor-pointer py-4"
            >
              {logoutIcon}
            </div>
          </ul>
        </div>
      </div>

      {/* Sidebar 2 */}
      <div
        className={`h-screen sidebar bg-white w-60 sidebar-nav-menu ${
          visible ? "p-2" : "inset-0 transform -translate-x-4 overflow-hidden"
        }`}
        style={{
          height: "100%",
          marginLeft: 64,
          transition: "all 0.3s",
          width: visible ? "" : "0px",
          opacity: visible ? "1" : "0",
          maxHeight: "100vh",
          overflowY: "auto",
        }}
      >
        <div className="flex justify-between items-center w-full">
          <p className="truncate text-3xl text-gray-700 font-medium h-9 flex items-center">
            {config.project.title_svg}
          </p>
          <span
            className="flex items-center mt-1 cursor-pointer"
            onClick={() => toggleSidebar()}
          >
            <svg
              width="20"
              height="14"
              viewBox="0 0 16 10"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M1.33333 10H10.5C10.9583 10 11.3333 9.625 11.3333 9.16667C11.3333 8.70833 10.9583 8.33333 10.5 8.33333H1.33333C0.875 8.33333 0.5 8.70833 0.5 9.16667C0.5 9.625 0.875 10 1.33333 10ZM1.33333 5.83333H8C8.45833 5.83333 8.83333 5.45833 8.83333 5C8.83333 4.54167 8.45833 4.16667 8 4.16667H1.33333C0.875 4.16667 0.5 4.54167 0.5 5C0.5 5.45833 0.875 5.83333 1.33333 5.83333ZM0.5 0.833333C0.5 1.29167 0.875 1.66667 1.33333 1.66667H10.5C10.9583 1.66667 11.3333 1.29167 11.3333 0.833333C11.3333 0.375 10.9583 0 10.5 0H1.33333C0.875 0 0.5 0.375 0.5 0.833333ZM14.9167 7.4L12.5167 5L14.9167 2.6C15.2417 2.275 15.2417 1.75 14.9167 1.425C14.5917 1.1 14.0667 1.1 13.7417 1.425L10.75 4.41667C10.425 4.74167 10.425 5.26667 10.75 5.59167L13.7417 8.58333C14.0667 8.90833 14.5917 8.90833 14.9167 8.58333C15.2333 8.25833 15.2417 7.725 14.9167 7.4Z"
                fill="#5B77A0"
              />
            </svg>
          </span>
        </div>
        <RenderSidebarItems items={selectedList} />
      </div>
    </div>
  )
}
