const menu = [
  {
    id: "dashboard",
    title: "dashboard",
    path: "/home/dashboard",
    isActive: false,
    icon: "DashboardIcon",
    children: [],
  },
  {
    id: "users",
    title: "users",
    path: "/home/users",
    isActive: false,
    icon: "GroupIcon",
    children: [],
  },
  {
    id: "catalog",
    title: "catalog",
    path: "/home/catalog",
    isActive: false,
    icon: "LocalMovies",
  },
  {
    id: "subscriptions",
    title: "subscriptions",
    path: "/home/subscriptions",
    isActive: false,
    icon: "Subscriptions",
  },
  {
    id: "roles",
    title: "roles",
    path: "/home/roles",
    isActive: false,
    icon: "GroupAdd",
  },
  {
    id: "reports",
    title: "reports",
    path: "/home/reports",
    isActive: false,
    icon: "AssessmentIcon",
  },
  {
    id: "content",
    title: "content",
    path: "/home/content",
    isActive: false,
    icon: "Devices",
  },

  {
    id: "settings",
    title: "settings",
    path: "/home/settings",
    isActive: true,
    icon: "SettingsIcon",
    children: [
      {
        id: "genres",
        title: "genres",
        path: "/home/settings/genres",
        isChild: true,
      },
      {
        id: "categories",
        title: "categories",
        path: "/home/settings/categories",
        isChild: true,
      },
      {
        id: "creators",
        title: "creators",
        path: "/home/settings/creators",
        isChild: true,
      },
      {
        id: "tags",
        title: "tags",
        path: "/home/settings/tags",
        isChild: true,
      },
      {
        id: "templates",
        title: "templates",
        path: "/home/settings/templates",
        isChild: true,
      },
      {
        id: "integrations",
        title: "integrations",
        path: "/home/settings/integrations",
        isChild: true,
      },
      {
        id: "roles",
        title: "roles",
        path: "/home/settings/roles",
        isChild: true,
      },
      {
        id: "permission",
        title: "permission",
        path: "/home/settings/permission",
        isChild: true,
      },
    ],
  },
]

export default menu
