import ChangePasswordAlert from "../components/Alert/ChangePasswordAlert.jsx"
import AlertComponent from "../components/Alert/index.jsx"
import Layout from "../components/Layout"
import Sidebar from "../components/Sidebar/index.jsx"

export default function DashboardLayout({ children }) {
  return (
    <>
      <AlertComponent />
      <ChangePasswordAlert />

      <div>
        <Layout sidebar={<Sidebar />}>{children}</Layout>
      </div>
    </>
  )
}
