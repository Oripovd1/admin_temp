import FullScreenLoader from "../components/FullScreenLoader"
import Loadable from "react-loadable"

const Settings = Loadable({
  loader: () => import("../views/settings/Index"),
  loading: FullScreenLoader,
})
const Dashboard = Loadable({
  loader: () => import("../views/dashboard/index"),
  loading: FullScreenLoader,
})
const Roles = Loadable({
  loader: () => import("../views/roles/Index.jsx"),
  loading: FullScreenLoader,
})
const RolesCreate = Loadable({
  loader: () => import("../views/roles/Create"),
  loading: FullScreenLoader,
})
const Permission = Loadable({
  loader: () => import("../views/permission"),
  loading: FullScreenLoader,
})
const PermissionCreate = Loadable({
  loader: () => import("../views/permission/Create"),
  loading: FullScreenLoader,
})
const Profile = Loadable({
  loader: () => import("../views/profile"),
  loading: FullScreenLoader,
})

export default [
  {
    component: Dashboard,
    path: "/dashboard",
    exact: true,
    title: "Dashboard",
    permission: "dashboard",
  },
  {
    component: Settings,
    path: "/settings",
    exact: true,
    title: "Settings",
    permission: "dashboard",
  },
  {
    component: Roles,
    path: "/settings/roles",
    exact: true,
    title: "Roles",
    permission: "dashboard",
  },
  {
    component: RolesCreate,
    path: "/settings/roles/create",
    exact: true,
    title: "RolesCreate",
    permission: "dashboard",
  },
  {
    component: RolesCreate,
    path: "/settings/roles/:id",
    exact: true,
    title: "RolesEdit",
    permission: "dashboard",
  },
  {
    component: Permission,
    path: "/settings/permission",
    exact: true,
    title: "Permission",
    permission: "dashboard",
  },
  {
    component: PermissionCreate,
    path: "/settings/permission/create",
    exact: true,
    title: "CreatePermission",
    permission: "dashboard",
  },
  {
    component: PermissionCreate,
    path: "/settings/permission/:id",
    exact: true,
    title: "CreatePermission",
    permission: "dashboard",
  },
  {
    component: Profile,
    path: "/profile",
    exact: true,
    title: "Profile",
    permission: "dashboard",
  },
].map((route) => ({
  ...route,
  path: `/home${route.path}`,
  id: Math.random() + new Date().getTime(),
}))
