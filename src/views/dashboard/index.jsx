import { useTranslation } from "react-i18next"
import Widgets from "../../components/Widgets"
import { LocalMovies, People } from "@material-ui/icons"
import { useMemo, useState } from "react"

const Dashboard = () => {
  const { t } = useTranslation()
  const [widgetData, setWidgetData] = useState({})

  const computedWidgetsData = useMemo(() => {
    return [
      {
        title: "Фильмы",
        icon: LocalMovies,
        number: 12,
      },
      {
        title: "Сериалы",
        icon: LocalMovies,
        number: 110823,
      },
      {
        title: "Мультфильмы",
        icon: LocalMovies,
        number: 75,
      },
      {
        title: "Клиенты",
        icon: People,
        number: 110823,
      },
    ]
  }, [widgetData])

  // const fetchWidgetData = () => {
  //   axios.get("/entity-count").then((res) => setWidgetData(res))
  // }

  return (
    <div>
      <div className="p-6">
        <Widgets data={computedWidgetsData} />
      </div>
    </div>
  )
}

export default Dashboard
