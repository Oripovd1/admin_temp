import Form from '../../components/Form/Index'
import Button from "../../components/Buttons/index"
import Input from "../../components/Input/index"
import { Formik } from "formik";
import * as Yup from "yup";

export default function Forms () {

  const validationSchema = Yup.object({
    name: Yup.string().required('Required name'),
    age: Yup.string().required('Required age')
  })

  return (
    <div className="p-4">
      <div className="bg-white p-4 rounded-lg">
        {/* <Form onSubmit={val => console.log(val)} rules={rules}> */}
        <Formik
          initialValues={{}}
          validationSchema={validationSchema}
          onSubmit={val => console.log(val)}
        >
          {formik => (
            <form onSubmit={formik.handleSubmit}>
              {/* <Form.Input name="name" label="Name" {...formik.getFieldProps('name')} />
              <Form.Input name="age" label="Age" {...formik.getFieldProps('age')} /> */}
              <Form.Item formik={formik} label="Name" name="name">
                <Input
                  id="name"
                  type="text"
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className={
                    formik.errors.email && formik.touched.email
                      ? "border-red-600"
                      : ""
                  }
                  // {...formik.getFieldProps('name')}
                />
              </Form.Item>
              <Button type="submit" >Submit</Button>
            </form>
          )}
        </Formik>

        {/* <Formik
          initialValues={{ email: "" }}
          onSubmit={async values => {
            await new Promise(resolve => setTimeout(resolve, 500));
            alert(JSON.stringify(values, null, 2));
          }}
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email()
              .required("Required")
          })}
        >
          {props => {
            const {
              values,
              touched,
              errors,
              dirty,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
              handleReset
            } = props;
            return (
              <form onSubmit={handleSubmit}>
                <label htmlFor="email" style={{ display: "block" }}>
                  Email
                </label>
                <Input
                  id="email"
                  placeholder="Enter your email"
                  type="text"
                  {...props.getFieldProps('email')}
                  // style={{borderColor: errors.email && touched.email ?  '#dc3a37 !important' : 'unset'}}
                  // className={
                  //   errors.email && touched.email
                  //     ? "#dc3a37"
                  //     : ""
                  // }
                />
                  {errors.email && touched.email && (
                    <div className="text-red-600">{errors.email}</div>
                  )}

                <button type="submit" disabled={isSubmitting}>
                  Submit
                </button>

              </form>
            );
          }}
        </Formik> */}
      </div>
    </div>
  )
} 